package top.hmtools.wxmp.message;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jsonzou.jmockdata.JMockData;

import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.message.reply.model.EReplyMessages;

public class ReplyMessagesTest {

	private final Logger logger = LoggerFactory.getLogger(ReplyMessagesTest.class);
	private ObjectMapper objectMapper;




	@Test
	public void generateReplyMessageBean(){
		EReplyMessages[] values = EReplyMessages.values();
		for(EReplyMessages item:values){
			BaseMessage message = (BaseMessage)JMockData.mock(item.getClassName());
			this.printFormatedJson(item.getEventName()+"（json）", message);
			this.logger.info(item.getEventName()+"（xml）：\r\n{}",message.toXmlMsg());
		}
	}
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
}
