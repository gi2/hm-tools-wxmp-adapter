package top.hmtools.wxmp.message.customerService.model;

/**
 * 客服账号描述信息实体类
 * @author hybo
 *
 */
public class KfAccountParam {

	/**
	 * 客服账号
	 */
	private String kf_account;
	
	/**
	 * 客服昵称（本对象实例作为从微信侧获取的数据结果集时，用此属性）
	 */
	private String kf_nick;
	
	/**
	 * 客服ID
	 */
	private String kf_id;
	
	/**
	 * 客服头像
	 */
	private String kf_headimgurl;
	
	/**
	 * 客服昵称（本对象实例作为请求参数时，用此属性）
	 */
	private String nickname;
	
	/**
	 * 客服密码（MD5加密后？？）
	 */
	private String password;

	public String getKf_account() {
		return kf_account;
	}

	public void setKf_account(String kf_account) {
		this.kf_account = kf_account;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKf_nick() {
		return kf_nick;
	}

	public void setKf_nick(String kf_nick) {
		this.kf_nick = kf_nick;
	}

	public String getKf_id() {
		return kf_id;
	}

	public void setKf_id(String kf_id) {
		this.kf_id = kf_id;
	}

	public String getKf_headimgurl() {
		return kf_headimgurl;
	}

	public void setKf_headimgurl(String kf_headimgurl) {
		this.kf_headimgurl = kf_headimgurl;
	}

}
