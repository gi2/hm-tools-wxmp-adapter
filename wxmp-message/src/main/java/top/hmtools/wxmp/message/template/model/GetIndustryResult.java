package top.hmtools.wxmp.message.template.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

/**
 * Auto-generated: 2019-08-28 22:52:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetIndustryResult extends ErrcodeBean{

	private Industry primary_industry;
	private Industry secondary_industry;

	public Industry getPrimary_industry() {
		return primary_industry;
	}

	public void setPrimary_industry(Industry primary_industry) {
		this.primary_industry = primary_industry;
	}

	public Industry getSecondary_industry() {
		return secondary_industry;
	}

	public void setSecondary_industry(Industry secondary_industry) {
		this.secondary_industry = secondary_industry;
	}

	@Override
	public String toString() {
		return "GetIndustryResult [primary_industry=" + primary_industry + ", secondary_industry=" + secondary_industry
				+ ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}


}