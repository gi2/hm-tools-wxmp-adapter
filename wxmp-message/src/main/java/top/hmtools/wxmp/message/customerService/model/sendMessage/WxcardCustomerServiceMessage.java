package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理--客服消息--发送卡券
 * @author HyboWork
 *
 */
public class WxcardCustomerServiceMessage extends BaseSendMessageParam{

	private Wxcard wxcard;

	public Wxcard getWxcard() {
		return wxcard;
	}

	public void setWxcard(Wxcard wxcard) {
		this.wxcard = wxcard;
	}

	@Override
	public String toString() {
		return "WxcardCustomerServiceMessage [wxcard=" + wxcard + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	
}
