package top.hmtools.wxmp.message.group.model.event;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

@WxmpMessage(msgType = MsgType.event, event = Event.MASSSENDJOBFINISH)
public class GroupMessageSendEvent extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6834264476490487718L;
	@XStreamAlias(value = "MsgID")
	protected String msgID;
	
	@XStreamAlias(value = "Status")
	protected String status;
	
	@XStreamAlias(value = "TotalCount")
	protected String totalCount;
	
	@XStreamAlias(value = "FilterCount")
	protected String filterCount;
	
	@XStreamAlias(value = "SentCount")
	protected String sentCount;
	
	@XStreamAlias(value = "ErrorCount")
	protected String errorCount;
	
	@XStreamAlias(value = "CopyrightCheckResult",impl=CopyrightCheckResultType.class)
	protected CopyrightCheckResultType copyrightCheckResult;
	
	@XStreamAlias(value = "ArticleUrlResult",impl=ArticleUrlResult.class)
	protected	ArticleUrlResult articleUrlResult;

	@Override
	public void configXStream(XStream xStream) {

	}

	public String getMsgID() {
		return msgID;
	}

	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	public String getFilterCount() {
		return filterCount;
	}

	public void setFilterCount(String filterCount) {
		this.filterCount = filterCount;
	}

	public String getSentCount() {
		return sentCount;
	}

	public void setSentCount(String sentCount) {
		this.sentCount = sentCount;
	}

	public String getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(String errorCount) {
		this.errorCount = errorCount;
	}

	public CopyrightCheckResultType getCopyrightCheckResult() {
		return copyrightCheckResult;
	}

	public void setCopyrightCheckResult(CopyrightCheckResultType copyrightCheckResult) {
		this.copyrightCheckResult = copyrightCheckResult;
	}
	
	public ArticleUrlResult getArticleUrlResult() {
		return articleUrlResult;
	}

	public void setArticleUrlResult(ArticleUrlResult articleUrlResult) {
		this.articleUrlResult = articleUrlResult;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "XmlType [msgID=" + msgID + ", status=" + status + ", totalCount=" + totalCount + ", filterCount="
				+ filterCount + ", sentCount=" + sentCount + ", errorCount=" + errorCount + ", copyrightCheckResult="
				+ copyrightCheckResult + ", event=" + event + ", eventKey=" + eventKey + ", toUserName=" + toUserName
				+ ", fromUserName=" + fromUserName + ", createTime=" + createTime + ", msgType=" + msgType + ", msgId="
				+ msgId + "]";
	}

}
