package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 点击菜单跳转链接时的事件推送 <br>
 * 推送XML数据包示例：
 * <p>
 * {@code <xml>
<ToUserName><![CDATA[toUser]]></ToUserName>
<FromUserName><![CDATA[FromUser]]></FromUserName>
<CreateTime>123456789</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[VIEW]]></Event>
<EventKey><![CDATA[www.qq.com]]></EventKey>
<MenuId>MENUID</MenuId>
</xml> }
 * </p>
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType = MsgType.event,event=Event.VIEW)
public class ViewEventMessage extends BaseEventMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6509389634835271479L;

	/**
	 * 指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。
	 */
	@XStreamAlias("MenuId")
	private String menuId;

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "ViewEventMessage [menuId=" + menuId + ", event=" + event + ", eventKey=" + eventKey + ", toUserName="
				+ toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime + ", msgType=" + msgType
				+ ", msgId=" + msgId + "]";
	}
	
	
}
