package top.hmtools.wxmp.user.model;

import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class TagListResult extends ErrcodeBean {

	private List<Long> tagid_list;

	public List<Long> getTagid_list() {
		return tagid_list;
	}

	public void setTagid_list(List<Long> tagid_list) {
		this.tagid_list = tagid_list;
	}

	@Override
	public String toString() {
		return "TagListResult [tagid_list=" + tagid_list + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
