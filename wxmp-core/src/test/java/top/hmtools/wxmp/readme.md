#### 说明
1. 本包是对`top.hmtools.wxmp.core`package下的类进行单元测试示例。
2. `top.hmtools.wxmp.BaseTest`是核心包的请求微信接口的基本使用流程示例。
3. `top.hmtools.wxmp.core.access_handle.DefaultAccessTokenHandleTest`是核心包获取access token的基本示例。