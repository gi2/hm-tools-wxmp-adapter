package top.hmtools.httpclient.hm2020test;

import java.io.File;

public class GetTokenParam {

	private String api_key;
	
	private String api_secret;
	
	private String return_url;
	
	private String notify_url;
	
	private String biz_no;
	
	private String comparison_type;
	
	private String uuid;
	
	private File image_ref1;

	public String getApi_key() {
		return api_key;
	}

	public void setApi_key(String api_key) {
		this.api_key = api_key;
	}

	public String getApi_secret() {
		return api_secret;
	}

	public void setApi_secret(String api_secret) {
		this.api_secret = api_secret;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getBiz_no() {
		return biz_no;
	}

	public void setBiz_no(String biz_no) {
		this.biz_no = biz_no;
	}

	public String getComparison_type() {
		return comparison_type;
	}

	public void setComparison_type(String comparison_type) {
		this.comparison_type = comparison_type;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public File getImage_ref1() {
		return image_ref1;
	}

	public void setImage_ref1(File image_ref1) {
		this.image_ref1 = image_ref1;
	}
	
	
}
